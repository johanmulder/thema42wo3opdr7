package nl.hanze.web.gba.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NatuurlijkPersoon implements Serializable
{
	private long bsn;
	private char geslacht;
	private String initialen;
	private String achternaam;
	private String straatnaam;
	private int nummer;
	private String postcode;
	private String woonplaats;

	/**
	 * @return the bsn
	 */
	public long getBsn()
	{
		return bsn;
	}

	/**
	 * @param bsn the bsn to set
	 */
	public void setBsn(long bsn)
	{
		this.bsn = bsn;
	}

	/**
	 * @return the geslacht
	 */
	public char getGeslacht()
	{
		return geslacht;
	}

	/**
	 * @param geslacht the geslacht to set
	 */
	public void setGeslacht(char geslacht)
	{
		this.geslacht = geslacht;
	}

	/**
	 * @return the initialen
	 */
	public String getInitialen()
	{
		return initialen;
	}

	/**
	 * @param initialen the initialen to set
	 */
	public void setInitialen(String initialen)
	{
		this.initialen = initialen;
	}

	public String getAchternaam()
	{
		return achternaam;
	}

	public void setAchternaam(String achternaam)
	{
		this.achternaam = achternaam;
	}

	public String getStraatnaam()
	{
		return straatnaam;
	}

	public void setStraatnaam(String straatnaam)
	{
		this.straatnaam = straatnaam;
	}

	public int getNummer()
	{
		return nummer;
	}

	public void setNummer(int nummer)
	{
		this.nummer = nummer;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

	public String getWoonplaats()
	{
		return woonplaats;
	}

	public void setWoonplaats(String woonplaats)
	{
		this.woonplaats = woonplaats;
	}
}
