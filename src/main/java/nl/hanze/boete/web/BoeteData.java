package nl.hanze.boete.web;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class BoeteData implements Serializable
{
	private int snelheid;
	private int maxSnelheid;
	private Date tijdstip;
	private String kenteken;

	public int getSnelheid()
	{
		return snelheid;
	}

	public void setSnelheid(int snelheid)
	{
		this.snelheid = snelheid;
	}

	public int getMaxSnelheid()
	{
		return maxSnelheid;
	}

	public void setMaxSnelheid(int maxSnelheid)
	{
		this.maxSnelheid = maxSnelheid;
	}

	public Date getTijdstip()
	{
		return tijdstip;
	}

	public void setTijdstip(Date tijdstip)
	{
		this.tijdstip = tijdstip;
	}

	public String getKenteken()
	{
		return kenteken;
	}

	public void setKenteken(String kenteken)
	{
		this.kenteken = kenteken;
	}

}
