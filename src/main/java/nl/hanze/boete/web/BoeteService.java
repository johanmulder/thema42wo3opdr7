package nl.hanze.boete.web;

import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.hanze.boete.BoeteVerwerkingsBean;

@WebService(serviceName = "BoeteService")
public class BoeteService
{
	@EJB
	private BoeteVerwerkingsBean bvb;
    
	@WebMethod
	@WebResult(name = "resultaat")
	public String verwerkBoete(@WebParam(name = "boeteParameters") BoeteData boeteData)
	{
		try
		{
			bvb.verwerkBoete(boeteData);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "OK";
	}
}
