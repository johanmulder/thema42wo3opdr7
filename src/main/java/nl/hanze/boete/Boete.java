package nl.hanze.boete;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Boete implements Serializable
{
	private double bedrag;
	private int boeteZwaartePunt;

	public double getBedrag()
	{
		return bedrag;
	}

	public void setBedrag(double bedrag)
	{
		this.bedrag = bedrag;
	}

	public int getBoeteZwaartePunt()
	{
		return boeteZwaartePunt;
	}

	public void setBoeteZwaartePunt(int boeteZwaartePunt)
	{
		this.boeteZwaartePunt = boeteZwaartePunt;
	}
}
