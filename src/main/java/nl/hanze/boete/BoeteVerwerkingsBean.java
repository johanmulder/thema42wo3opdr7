package nl.hanze.boete;

import java.io.IOException;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.ObjectMessage;
import javax.jms.Queue;

import nl.hanze.boete.web.BoeteData;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;
import nl.hanze.web.rdw.service.Info;
import nl.hanze.web.rdw.service.ThesaurusService;

@Stateless
@Local
public class BoeteVerwerkingsBean
{
	@Resource(lookup = "jms/boetebepaling")
	private Queue boeteBepalingQueue;
    @Inject
    private JMSContext context;
    @Inject
    private GBA gba;
    @Inject
    private ThesaurusService rdw;
    
    /**
     * Verwerk een boete.
     * @param boeteParameters
     */
	public void verwerkBoete(BoeteData boeteData)
	{
		try
		{
			// Initiële conversie van boete data naar parameters.
			BoeteParameters boeteParameters = boeteDataToParameters(boeteData);
			// Haal het BSN-nummer voor het betreffende kenteken.
			NatuurlijkPersoon np = getNatuurlijkPersoonVoorKenteken(boeteData.getKenteken());
			boeteParameters.setNatuurlijkPersoon(np);
			// Haal de zwaartepunten op voor de betreffende persoon.
			boeteParameters.setZwaartepunten(getZwaartePuntenVoorBsn(np.getBsn()));
			
			// Als laatste, publiceer de boete.
			publiceerBoete(boeteParameters);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Haal het BSN-nummer voor het betreffende kenteken op.
	 * @param kenteken
	 * @return
	 * @throws IOException 
	 */
	private NatuurlijkPersoon getNatuurlijkPersoonVoorKenteken(String kenteken) throws IOException
	{
		Info info = rdw.getThesaurusPort().getInfo(kenteken);
		return gba.getNatuurlijkPersoon(info.getBsn());
	}
	
	/**
	 * Converteer de boete data naar een BoeteParameters object.
	 * @param boeteData
	 * @return
	 */
	private BoeteParameters boeteDataToParameters(BoeteData boeteData)
	{
		BoeteParameters boeteParameters = new BoeteParameters();
		boeteParameters.setSnelheid(boeteData.getSnelheid());
		boeteParameters.setSnelheidsLimiet(boeteData.getMaxSnelheid());
		boeteParameters.setTijdstip(boeteData.getTijdstip());
		return boeteParameters;
	}
	
	private int getZwaartePuntenVoorBsn(long bsn)
	{
		// TODO: invullen.
		return 0;
	}
	
	private void publiceerBoete(BoeteParameters boeteParameters)
	{
		ObjectMessage message = context.createObjectMessage(boeteParameters);
        context.createProducer().send(boeteBepalingQueue, message);
        System.err.println("Published message");
	}
}
