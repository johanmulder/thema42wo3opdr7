package nl.hanze.boete;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import nl.hanze.web.gba.domain.NatuurlijkPersoon;

import com.google.gson.Gson;

public class GBA
{
	public NatuurlijkPersoon getNatuurlijkPersoon(long bsn) throws IOException
	{
		Gson gson = new Gson();
		String url = "http://appel.local:8080/thema42wo3opdr2/GBA?action=get&bsn=" + bsn;
		String jsonData = readUrl(url);
		GbaInfo g = gson.fromJson(jsonData, GbaInfo.class);
		return g.getNatuurlijkPersoon();
	}

	private static String readUrl(String urlString) throws IOException
	{
		URL url = null;
		try
		{
			url = new URL(urlString);
		}
		catch (MalformedURLException e)
		{
			// De url is altijd goed.
		}
		
		try (InputStream is = url.openStream())
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		}
	}
}
