package nl.hanze.boete;

import java.io.Serializable;
import java.util.Date;

import nl.hanze.web.gba.domain.NatuurlijkPersoon;

@SuppressWarnings("serial")
public class BoeteParameters implements Serializable
{
	private int snelheid;
	private int snelheidsLimiet;
	private int zwaartepunten;
	private Date tijdstip;
	private NatuurlijkPersoon natuurlijkPersoon;
	private Boete boete;

	public BoeteParameters()
	{
	}

	public BoeteParameters(int snelheid, int snelheidsLimiet, int zwaartepunten,
			NatuurlijkPersoon np, Date tijdstip)
	{
		this.snelheid = snelheid;
		this.snelheidsLimiet = snelheidsLimiet;
		this.zwaartepunten = zwaartepunten;
		this.tijdstip = tijdstip;
		this.natuurlijkPersoon = np;
	}

	public int getSnelheid()
	{
		return snelheid;
	}

	public void setSnelheid(int snelheid)
	{
		this.snelheid = snelheid;
	}

	public int getZwaartepunten()
	{
		return zwaartepunten;
	}

	public void setZwaartepunten(int zwaartepunten)
	{
		this.zwaartepunten = zwaartepunten;
	}

	public int getSnelheidsLimiet()
	{
		return snelheidsLimiet;
	}

	public void setSnelheidsLimiet(int snelheidsLimiet)
	{
		this.snelheidsLimiet = snelheidsLimiet;
	}

	public Date getTijdstip()
	{
		return tijdstip;
	}

	public void setTijdstip(Date tijdstip)
	{
		this.tijdstip = tijdstip;
	}

	public NatuurlijkPersoon getNatuurlijkPersoon()
	{
		return natuurlijkPersoon;
	}

	public void setNatuurlijkPersoon(NatuurlijkPersoon natuurlijkPersoon)
	{
		this.natuurlijkPersoon = natuurlijkPersoon;
	}

	public Boete getBoete()
	{
		return boete;
	}

	public void setBoete(Boete boete)
	{
		this.boete = boete;
	}
}