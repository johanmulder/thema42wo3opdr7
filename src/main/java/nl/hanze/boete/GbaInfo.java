package nl.hanze.boete;

import nl.hanze.web.gba.domain.NatuurlijkPersoon;

public class GbaInfo
{
	private NatuurlijkPersoon natuurlijkpersoon;

	public NatuurlijkPersoon getNatuurlijkPersoon()
	{
		return natuurlijkpersoon;
	}

	public void setNatuurlijkPersoon(NatuurlijkPersoon natuurlijkPersoon)
	{
		this.natuurlijkpersoon = natuurlijkPersoon;
	}
}
