package gson;

import nl.hanze.boete.GbaInfo;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

public class GsonTest
{

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void test()
	{
		String json = "{\"natuurlijkpersoon\": {\"bsn\": \"123\",\"geslacht\": \"M\",\"initialen\": \"I\",\"achternaam\": \"Achternaam\",\"straatnaam\": \"Straat\",\"nummer\": \"123\",\"postcode\": \"1234AA\",\"woonplaats\": \"Test\"}}";
		Gson gson = new Gson();
		GbaInfo g = gson.fromJson(json, GbaInfo.class);
		org.junit.Assert.assertEquals(123L, g.getNatuurlijkPersoon().getBsn());
	}

}
